﻿namespace $$PROJECT$$.Controllers;

[Route("[controller]")]
[ApiController]
public class ValuesController : ControllerBase
{
  private readonly $$DBNAME$$Context _db;
  public ValuesController($$DBNAME$$Context db) => _db = db;
  
  [HttpGet("$$TABLE$$")]
  public object Get$$TABLE$$()
  {
    Console.WriteLine($"{DateTime.Now:HH:mm:ss} Get$$TABLE$$");
    try
    {
  	  int nr = _db.$$TABLE$$.Count();
  	  return new { IsOk = true, Nr = nr };
    }
    catch (Exception exc)
    {
  	  return new { IsOk = false, Nr = -1, Error = exc.Message };
    }
  }

}
