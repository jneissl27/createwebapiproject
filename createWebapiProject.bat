@echo off
setlocal EnableDelayedExpansion
set versionScript=v6.2.2
set versionDate=2022-06-10
echo [93mcreateWebApiProject %versionScript% / %versionDate%[0m

if "%1"=="" (
  echo [95musage:[0m createWebApiProject solutionName [pathToDbFile] [tableName] [targetFolder]
  echo    [96msolutionName[0m: Name of the solution and name of folder
  echo    [96mpathToDbFile[0m: If set, an Entity data model will be generated. Database has to reside in local folder or absolute path given.
  echo    [96mtableName[0m:    For this table a Get-WebService is generated. Default: Categories
  echo    [96mtargetFolder[0m: Folder where to generate the project to. Default: Subfolder with solution name in current folder
  exit/b
)
set currentPath=%cd%
set solutionName=%1
set projectName=%1
set dbFile=%2
set tableName=%3
set targetFolder=%4
set isAbsoluteDbPath=false
set httpPort=5000
set httpsPort=5001

echo [7m---------------------------------------[0m
echo [95m.Net Core WebApi project create script[0m
echo [95m          !versionScript!, !versionDate!         [0m
echo [95m  (C)Robert Grueneis/HTL Grieskirchen [0m
echo [7m---------------------------------------[0m

::------------------------------------------------------------ check if second parameter is target folder
set secondParameterIsDatabase=F
if not "%dbFile%"=="" if "%tableName%"=="" if "%targetFolder%"=="" (
  if "!dbFile:~-4!"==".mdf" (set secondParameterIsDatabase=T)
  if "!dbFile:~-3!"==".db" (set secondParameterIsDatabase=T)
  if "!dbFile:~-7!"==".sqlite" (set secondParameterIsDatabase=T)
)
if "%targetFolder%"=="" if "%secondParameterIsDatabase%"=="F" (
  set targetFolder=!dbFile!
  set dbFile=
)

::------------------------------------------------------------ set versions
echo [95mCurrent dotnet version[0m:
dotnet --version
echo [95mCurrent dotnet-ef version[0m:
dotnet-ef --version
echo [7m---------------------------------------[0m
for /F "tokens=* USEBACKQ" %%F in (`dotnet --version`) do (
set temp=%%F
)
set netcoreVersion=%temp:~0,3%
for /F "tokens=* USEBACKQ" %%F in (`dotnet-ef --version`) do (
set efVersion=%%F
)
set netcoreVersionMajor=%netcoreVersion:~0,1%
set efVersionMajor=%efVersion:~0,1%
echo Using .Net Core:        [96m%netcoreVersion%[0m
echo Using Entity Framework: [96m%efVersion%[0m
if "%netcoreVersionMajor%" LSS "6" (
  echo [91mwrong version of DotNet Core[0m - at least version 6 required
  exit/b
)
if "%efVersionMajor%" LSS "6" (
  echo [91mmissing or wrong version of Entity Framework Core[0m - at least version 6 required
  if "%efVersion%"=="" set efVersion=6.0
  echo if not available install with: [92mdotnet tool install --global dotnet-ef --version !efVersion![0m
  echo if is  available update  with: [92mdotnet tool update  --global dotnet-ef --version !efVersion![0m
  echo perhaps you have to add the path of your user-home /.dotnet/tools
  exit/b
)
::echo [95mAll dotnet versions[0m:
::dotnet --list-sdks
::echo [7m-----------------------[0m
echo [7m---------------------------------------[0m

::------------------------------------------------------------ set variables
if "%solutionName%"=="" (set solutionName=MyWebApi)
if "%projectName%"=="" (set projectName=%solutionName%)

set withDb=false
if not "%dbFile%"=="" set withDb=true

if %withDb%==false goto skipDb
  set fullMdfPath=%currentPath%\%dbFile%
  if not x%dbFile::=%==x%dbFile% (
    set fullMdfPath=%dbFile%
	set isAbsoluteDbPath=true
  )
  if not exist "!fullMdfPath!" (
	echo [91mDb-File %fullMdfPath% does not exist![0m
	exit/b
  )
  if "%tableName%"=="" set tableName=Categories

set isMdf=true
if "%dbFile:~-3%"=="ite" set isMdf=false
if "%dbFile:~-3%"==".db" set isMdf=false

%currentPath%\ExtractDbName %fullMdfPath%
set /p dbName=<tempDbName.txt
if %dbName%==Northwnd set dbName=Northwind
del /f tempDbName.txt
::exit /b

:skipDb
set url=http://localhost:%httpPort%/WeatherForecast

::------------------------------------------------------------ log setup
echo [95msolution[0m      --^> [96m%solutionName%[0m
echo [95mproject[0m       --^> [96m%projectName%[0m
if %withDb%==true (echo [95mdatabase[0m      --^> [96m%dbFile%[0m) else (echo without creating EFCore)
if not "%targetFolder%"=="" echo [95mtarget folder[0m --^> [96m%targetFolder%\%solutionName%[0m
echo [7m---------------------------------------[0m

if %withDb%==true (echo Database name = %dbName%)
if %withDb%==true (echo fullDbPath = %fullMdfPath%)

if "%targetFolder%"=="" (set targetFolder=".")
cd /d %targetFolder%

::------------------------------------------------------------ create solution and projects
mkdir %solutionName%
cd %solutionName%
set projectRoot="%cd%"
echo [92mCreating new solution[0m [96m%solutionName%[0m to [96m%projectRoot%[0m
::exit /b
dotnet new sln

::------------------------------------------------------------ create WebApi project and add to solution
mkdir %projectName%
cd %projectName%
echo [92mCreating new WebApi Project[0m [96m%projectName%[0m
:: dotnet new webapi --no-https
dotnet new webapi
cd..
echo [92mAdding project to solution[0m
dotnet sln %solutionName%.sln add %projectName%
cd %projectName%
echo [92mAdding package for Microsoft.Extensions.Logging.Debug (version %netcoreVersion%)[0m...
dotnet add package Microsoft.Extensions.Logging.Debug -v %netcoreVersion% -n
echo [92mAdding package for Microsoft.VisualStudio.Web.CodeGeneration.Design (version %netcoreVersion%)[0m...
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design -v %netcoreVersion% -n

echo [92mAdding package for Swashbuckle.AspNetCore (lastest version)[0m...
dotnet add package Swashbuckle.AspNetCore -n
echo [92mAdding package for GrueneisR.RestClientGenerator (lastest version)[0m...
dotnet add package GrueneisR.RestClientGenerator -n

echo [92mCreatinging folder for Dtos[0m
mkdir Dtos
echo namespace %projectName%.Dtos; > Dtos\YourDtosComeHere.cs
echo public class YourDtosComeHere {} >> Dtos\YourDtosComeHere.cs
echo [92mCreatinging folder for Services[0m
mkdir Services
echo namespace %projectName%.Services; > Services\YourServicesComeHere.cs
echo public class YourDtosComeHere {} >> Services\YourServicesComeHere.cs

if %withDb%==false goto adapting

::------------------------------------------------------------ create Db project and add to solution
cd..
set dbProject=%dbName%Db
mkdir %dbProject%
cd %dbProject%
echo [92mCreating new Db Class Library Project[0m [96m%dbProject%[0m
dotnet new classlib -f net%netcoreVersion%

echo [92mAdding package for EntityFrameworkCore (version %efVersion%)[0m...
dotnet add package Microsoft.EntityFrameworkCore -v %efVersion% -n
echo [92mAdding package for EntityFrameworkCore.Design (version %efVersion%)[0m...
dotnet add package Microsoft.EntityFrameworkCore.Design -v %efVersion% -n
echo [92mAdding package for EntityFrameworkCore.Tools (version %efVersion%)[0m...
dotnet add package Microsoft.EntityFrameworkCore.Tools -v %efVersion% -n

if %isMdf%==true echo [92mAdding package for EntityFrameworkCore.SqlServer (version %efVersion%)[0m...
if %isMdf%==true dotnet add package Microsoft.EntityFrameworkCore.SqlServer -v %efVersion% -n

if %isMdf%==false echo [92mAdding package for EntityFrameworkCore.Sqlite (version %efVersion%)[0m...
if %isMdf%==false dotnet add package Microsoft.EntityFrameworkCore.Sqlite -v %efVersion% -n

echo database path is absolute: [96m%isAbsoluteDbPath%[0m
set targetMdfPath=%cd%\%dbFile%
if %isAbsoluteDbPath%==true set targetMdfPath=%fullMdfPath%
if %isAbsoluteDbPath%==false echo [92mCopying Db-File[0m [96m%fullMdfPath%[0m
if %isAbsoluteDbPath%==false copy %fullMdfPath% !targetMdfPath!

echo [92mCreating Entity classes[0m for [96m%targetMdfPath%[0m
if %isMdf%==true dotnet ef dbcontext scaffold "Server=(LocalDB)\mssqllocaldb;attachdbfilename=!targetMdfPath!;integrated security=True" Microsoft.EntityFrameworkCore.SqlServer -f -c %dbName%Context
if %isMdf%==false dotnet ef dbcontext scaffold "data source=!targetMdfPath!" Microsoft.EntityFrameworkCore.Sqlite -f -c %dbName%Context

if %isMdf%==false echo [96m%dbName%Context.cs[0m
if %isMdf%==false %currentPath%\ReplaceInFile .\%dbName%Context.cs .\%dbName%Context.cs ValueGeneratedNever=ValueGeneratedOnAdd

set dbProjectPath=%cd%\%dbProject%.csproj
cd..
echo [92mAdding Db project to solution[0m
dotnet sln %solutionName%.sln add %dbProject%
cd %projectName%
echo [92mAdding reference to Db project[0m
dotnet add reference %dbProjectPath%


:adapting
::------------------------------------------------------------ adapting
echo [92mAdapting original files (to automatically enable CORS)[0m
echo [96mProgram.cs[0m
if %withDb%==false %currentPath%\ReplaceInFile %currentPath%\_templates\Program.cs .\Program.cs $$PROJECT$$=%projectName%;$$PROJECTROOT$$=%projectRoot%;$$HTTP_PORT$$=%httpPort%;$$VERSIONSCRIPT$$=%versionScript%;$$VERSIONDATE$$=%versiondate%
if %withDb%==false %currentPath%\ReplaceInFile %currentPath%\_templates\launchSettings_base.json Properties\launchSettings.json $$PROJECT$$=%projectName%;$$HTTP_PORT$$=%httpPort%;$$HTTPS_PORT$$=%httpsPort%

echo [96mappsettings.Development.json[0m
%currentPath%\ReplaceInFile %currentPath%\_templates\appsettings.Development.json appsettings.Development.json $$NOTHING$$=$$NITSCHEWO$$

if %withDb%==false goto finished

set url=http://localhost:%httpPort%/Values/%tableName%

set program=Program_Mdf.cs
if %isMdf%==false set program=Program_Sqlite.cs
%currentPath%\ReplaceInFile %currentPath%\_templates\!program! .\Program.cs $$PROJECT$$=%projectName%;$$DBPROJECT$$=%dbProject%;$$PROJECTROOT$$=%projectRoot%;$$DBNAME$$=%dbName%;$$DBFILE$$=%dbFile%;$$FULLDBPATH$$=%fullMdfPath%;$$HTTP_PORT$$=%httpPort%;$$VERSIONSCRIPT$$=%versionScript%;$$VERSIONDATE$$=%versiondate%

echo [96mappsettings.json[0m
set appsettings=appsettings_mdf.json
if %isMdf%==false set appsettings=appsettings_sqlite.json
%currentPath%\ReplaceInFile %currentPath%\_templates\!appsettings! appsettings.json $$PROJECT$$=%projectName%;$$DBNAME$$=%dbName%;$$FULLDBPATH$$=%fullMdfPath%;$$FILENAME$$_FILENAMEONLY=%fullMdfPath%;\=\\

echo [96mValuesController.cs[0m
%currentPath%\ReplaceInFile %currentPath%\_templates\ValuesController.cs Controllers\ValuesController.cs $$PROJECT$$=%projectName%;$$DBPROJECT$$=%dbProject%;$$DBNAME$$=%dbName%;$$TABLE$$=%tableName%
echo [96mRemove unused files[0m
echo Controllers\WeatherForecastController.cs
del Controllers\WeatherForecastController.cs
echo WeatherForecast.cs
del WeatherForecast.cs

echo [96mlaunchSettings.json[0m
%currentPath%\ReplaceInFile %currentPath%\_templates\launchSettings.json Properties\launchSettings.json $$PROJECT$$=%projectName%;$$TABLE$$=%tableName%;$$HTTP_PORT$$=%httpPort%;$$HTTPS_PORT$$=%httpsPort%

echo [96mGlobalUsings.cs[0m
%currentPath%\ReplaceInFile %currentPath%\_templates\GlobalUsings.cs GlobalUsings.cs $$PROJECT$$=%projectName%;$$DBPROJECT$$=%dbProject%

:finished

echo [96mExtensionMethods.cs[0m
%currentPath%\ReplaceInFile %currentPath%\_templates\ExtensionMethods.cs ExtensionMethods.cs $$PROJECT$$=%projectName%

echo [7m-----------------------[0m
echo [95mInstallation finished![0m
echo [7m-----------------------[0m
echo Press a key to start the project and open browser with url [96m!url![0m ...
pause >nul

start dotnet watch run
