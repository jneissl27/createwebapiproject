versionScript="v2.1"
versionDate="2022-09-22"
echo "createWebApiProject.sh $versionScript / $versionDate"
echo "  by Janik Neißl/5b HTL Grieskirchen"

checkParams() {
    if [[ -z $1 ]]; then
        echo "Usage: createWebApiProject solutionName [pathToDbFile] [tableName] [targetFolder]"
        echo "  solutionName: Name of the solution and folder"
        echo "  pathToDbFile: If set, an Entity data model will be generated. Can be relative or absolute."
        echo "  tableName:    For this table a Get-WebService is generated. Default: Categories"
        echo "  targetFolder: Folder to generate the project in. Default: Subfolder of calling directory, named like the solution."
        exit 1
    fi;
}

checkParams "$@"

extractDbName() {
    # source: https://stackoverflow.com/a/2664746

    # strip path
    local relativeName="${1##*/}"

    # strip extension
    local dbName="${relativeName%.*}"

    # capitalize first letter
    # source: https://www.linuxquestions.org/questions/programming-9/bash-scripting-capitalizing-first-letter-or-each-word-in-a-string-268182/
    local dbName="${dbName[*]^}"

    if [[ $dbName == "Northwnd" ]]; then
        local dbName="Northwind"
    fi;

    echo $dbName
}

# Danke Github Copilot
replaceInFile() {
    local search=$1
    local replace=$2
    sed -i "s|${search}|${replace}|g" "$3"
}

if [[ "$OSTYPE" == "darwin"* ]]; then
    replaceInFile() {
        local search=$1
        local replace=$2
        sed -i ".klumpatmac" "s|$search|$replace|g" "$3"
    }
fi;

currentPath=$(pwd)
solutionName=$1
projectName=$solutionName
dbFile=$2
tableName=$3
targetFolder=$4
isAbsoluteDbPath=false
httpPort=5000
httpsPort=5001

#------------------------------------------------------------ set versions
netcoreVersion=$(dotnet --version)
netcoreStatus=$?
if ! [[ $netcoreStatus -eq 0 ]]; then
    echo "DotNet Core not found. At least version 6 required."
    exit 1
fi;

efVersion=$(dotnet-ef --version)
efStatus=$?
if ! [[ $efStatus -eq 0 ]]; then
    echo "Entity Framework Core not found. At least version 6 required."
    exit 1
fi;

# source: https://stackoverflow.com/a/6245802
netcoreVersion=$(echo "$netcoreVersion" | awk -F \. '{print $1"."$2}')
netcoreVersionMajor=$(echo "$netcoreVersion" | awk -F \. '{print $1}')
efVersion=$(echo "$efVersion" | grep -o '[0-9]*\.[0-9]*\.[0-9]*')
efVersionMajor=$(echo "$efVersion" | awk -F \. '{print $1}')

echo "Current dotnet version: $(dotnet --version)"
echo "Current dotnet-ef version: $efVersion"
echo "---------------------------------------"

echo "Using .Net Core:                  $netcoreVersion"
echo "Using Entity Framework Version:   $efVersion"

if [[ $netcoreVersionMajor -lt 6 ]]; then
    echo "Wrong version of DotNet Core - at least version 6 required"
    exit 1
fi;

if [[ $efVersionMajor -lt 6 ]]; then
    echo "Missing or wrong versin of Entity Framework Core - at least version 6 required"
    exit 1
fi;

echo "---------------------------------------"


#------------------------------------------------------------ set variables
if [[ -z $solutionName ]]; then
    solutionName=MyWebApi
    projectName=$solutionName
fi;

withDb=false
if [[ -n $dbFile ]]; then
    withDb=true
fi;

if [[ $withDb == true ]]; then
    if [[ $dbFile == /* ]]; then
        fullDbPath=$dbFile
        isAbsoluteDbPath=true
    else
        fullDbPath=$currentPath/$dbFile
    fi;

    if ! [[ -e $fullDbPath ]]; then
        echo "File $fullDbPath does not exist!"
    fi;

    if [[ -z $tableName ]]; then
        tableName="Categories"
    fi;

    dbName="$(extractDbName "$fullDbPath")"
fi;

url="http://localhost:$httpPort/swagger"


#------------------------------------------------------------ log setup
echo "solution      --> $solutionName"
echo "project       --> $projectName"

if [[ $withDb == true ]]; then
    echo "database      --> $dbFile"
else
    echo "without creating EFCore"
fi;

if [[ -n $targetFolder ]]; then
    echo "target folder --> $targetFolder/$solutionName"
fi;

echo "---------------------------------------"

if [[ $withDb == true ]]; then
    echo "Database name = $dbName"
    echo "fullDbPath = $fullDbPath"
fi;

if [[ -z $targetFolder ]]; then
    targetFolder="."
fi;

mkdir -p $targetFolder
cd $targetFolder || { echo "cd failed"; exit 1; }


#------------------------------------------------------------ create solution and projects
mkdir $solutionName
cd $solutionName || { echo "cd failed"; exit 1; }
projectRoot=$(pwd)
echo "Creating new solution $solutionName to $projectRoot"
dotnet new sln


#------------------------------------------------------------ create WebApi project and add to solution
mkdir $projectName
cd $projectName || { echo "cd failed"; exit 1; }
echo "Creating new WebApi Project $projectName"
dotnet new webapi

cd ..
echo "Adding project to solution"
dotnet sln $solutionName.sln add $projectName

cd $projectName || { echo "cd failed"; exit 1; }
echo "Adding package for Microsoft.Extensions.Logging.Debug (version $netcoreVersion)..."
dotnet add package Microsoft.Extensions.Logging.Debug -v "$netcoreVersion" -n
echo "Adding package for Microsoft.VisualStudio.Web.CodeGeneration.Design (version $netcoreVersion)..."
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design -v "$netcoreVersion" -n

echo "Adding package for Swashbuckle.AspNetCore (latest version)..."
dotnet add package Swashbuckle.AspNetCore -n
echo "Adding package for GrueneisR.RestClientGenerator (latest version)..."
dotnet add package GrueneisR.RestClientGenerator -n

mkdir Dtos
echo "namespace ${projectName}.Dtos;" > Dtos/YourDtosComeHere.cs
echo "public class YourDtosComeHere {}" >> Dtos/YourDtosComeHere.cs

mkdir Services
echo "namespace ${projectName}.Services;" > Services/YourServicesComeHere.cs
echo "public class YourServicesComeHere {}" >> Services/YourServicesComeHere.cs

#------------------------------------------------------------ create Db project and add to solution

dbProject() {
    cd ..
    dbProject="${dbName}Db"
    mkdir "$dbProject"
    cd "$dbProject" || { echo "cd failed"; exit 1; }
    echo "echo Creating new Db Class Library Project $dbProject"
    dotnet new classlib -f "net${netcoreVersion}"

    echo "Adding package for EntityFrameworkCore (version $efVersion)..."
    dotnet add package Microsoft.EntityFrameworkCore -v "$efVersion" -n
    echo "Adding package for EntityFrameworkCore.Design (version $efVersion)..."
    dotnet add package Microsoft.EntityFrameworkCore.Design -v "$efVersion" -n
    echo "Adding package for EntityFrameworkCore.Tools (version $efVersion)..."
    dotnet add package Microsoft.EntityFrameworkCore.Tools -v "$efVersion" -n
    echo "Adding package for EntityFrameworkCore.Sqlite (version $efVersion)..."
    dotnet add package Microsoft.EntityFrameworkCore.Sqlite -v "$efVersion" -n

    echo "Database Path is absolute: $isAbsoluteDbPath"
    targetDbPath="$(pwd)/${dbFile}"
    if [[ $isAbsoluteDbPath == true ]]; then
        targetDbPath=$fullDbPath
    else
        echo "Copying Db-File to $fullDbPath"
        cp "$fullDbPath" "$targetDbPath"
    fi

    echo "Creating Entity classes for $targetDbPath"
    dotnet ef dbcontext scaffold "data source=$targetDbPath" Microsoft.EntityFrameworkCore.Sqlite -f -c "${dbName}Context"

    echo "${dbName}Context.cs"
    replaceInFile "ValueGeneratedNever", "ValueGeneratedOnAdd", "./${dbName}Context.cs"

    cd ..
    echo "Adding Db project to solution"
    dotnet sln "${solutionName}.sln" add "$dbProject"
    cd $projectName || { echo "cd failed"; exit 1; }
    dbProjectPath="../${dbProject}/${dbProject}.csproj"
    echo "Adding reference to Db project"
    dotnet add reference "$dbProjectPath"
}

if [[ $withDb == true ]]; then
    dbProject
fi;


#------------------------------------------------------------ adapting
echo "Adapting original files (to automatically enable CORS)"
echo "Program.cs"

echo "appsettings.Development.json"
cp "${currentPath}/_templates/appsettings.Development.json" appsettings.json
replaceInFile '\$\$NOTHING\$\$' 'NITSCHEWO' appsettings.json

echo "ExtensionMethods.cs"
cp "${currentPath}/_templates/ExtensionMethods.cs" ExtensionMethods.cs
replaceInFile '\$\$PROJECT\$\$' $projectName ExtensionMethods.cs

if [[ $withDb == false ]]; then
    cp "${currentPath}/_templates/Program.cs" ./Program.cs
    replaceInFile '\$\$PROJECT\$\$' $projectName ./Program.cs
    replaceInFile '\$\$PROJECTROOT\$\$' "$projectRoot" ./Program.cs
    replaceInFile '\$\$HTTP_PORT\$\$' $httpPort ./Program.cs
    replaceInFile '\$\$VERSIONSCRIPT\$\$' $versionScript ./Program.cs

    cp "${currentPath}/_templates/launchSettings_base.json" Properties/launchSettings.json
    replaceInFile '\$\$PROJECT\$\$' $projectName Properties/launchSettings.json
    replaceInFile '\$\$HTTP_PORT\$\$' $httpPort Properties/launchSettings.json
    replaceInFile '\$\$HTTPS_PORT\$\$' $httpsPort Properties/launchSettings.json
fi

if [[ $withDb == true ]]; then
    url="http://localhost:${httpPort}/Values/${tableName}"
    cp "${currentPath}/_templates/Program_Sqlite.cs" ./Program.cs
    replaceInFile '\$\$PROJECT\$\$' $projectName ./Program.cs
    replaceInFile '\$\$PROJECTROOT\$\$' "$projectRoot" ./Program.cs
    replaceInFile '\$\$DBPROJECT\$\$' "$dbProject" ./Program.cs
    replaceInFile '\$\$DBNAME\$\$' "$dbName" ./Program.cs
    replaceInFile '\$\$DBFILE\$\$' "$dbFile" ./Program.cs
    replaceInFile '\$\$FULLDBPATH\$\$' "$fullDbPath" ./Program.cs
    replaceInFile '\$\$HTTP_PORT\$\$' $httpPort ./Program.cs
    replaceInFile '\$\$VERSIONSCRIPT\$\$' $versionScript ./Program.cs
    replaceInFile '\$\$VERSIONDATE\$\$' $versionDate ./Program.cs

    echo "appsettings.json"
    cp "${currentPath}/_templates/appsettings_sqlite.json" appsettings.json
    replaceInFile '\$\$PROJECT\$\$' $projectName appsettings.json
    replaceInFile '\$\$DBNAME\$\$' "$dbName" appsettings.json
    replaceInFile '\$\$FULLDBPATH\$\$' "$fullDbPath" appsettings.json
    replaceInFile '\$\$FILENAME\$\$' "$fullDbPath" appsettings.json

    echo "ValuesController.cs"
    cp "${currentPath}/_templates/ValuesController.cs" Controllers/ValuesController.cs
    replaceInFile '\$\$PROJECT\$\$' $projectName Controllers/ValuesController.cs
    replaceInFile '\$\$DBPROJECT\$\$' "$dbProject" Controllers/ValuesController.cs
    replaceInFile '\$\$DBNAME\$\$' "$dbName" Controllers/ValuesController.cs
    replaceInFile '\$\$TABLE\$\$' $tableName Controllers/ValuesController.cs

    echo "Removing unused files"
    echo "Controllers/WeatherForecastController.cs"
    rm Controllers/WeatherForecastController.cs
    echo "WeatherForecast.cs"
    rm WeatherForecast.cs

    echo "launchSettings.json"
    cp "${currentPath}/_templates/launchSettings.json" Properties/launchSettings.json
    replaceInFile '\$\$PROJECT\$\$' $projectName Properties/launchSettings.json
    replaceInFile '\$\$TABLE\$\$' $tableName Properties/launchSettings.json
    replaceInFile '\$\$HTTP_PORT\$\$' $httpPort Properties/launchSettings.json
    replaceInFile '\$\$HTTPS_PORT\$\$' $httpsPort Properties/launchSettings.json

    echo "GlobalUsings.cs"
    cp "${currentPath}/_templates/globalUsings.cs" GlobalUsings.cs
    replaceInFile '\$\$PROJECT\$\$' $projectName GlobalUsings.cs
    replaceInFile '\$\$DBPROJECT\$\$' "$dbProject" GlobalUsings.cs
fi;


echo "----------------------"
echo "Installation finished!"
echo "----------------------"

read -rs -n 1 -p "Press a key to start the project and open browser with url ${url}... "

dotnet watch run
