# createWebApiProject



## Getting started

This batch script for windows is used to create a C# WebApi backend, optionally including access to a database (formats Mdf and Sqlite are supported).
Note:
* CORS is activated in the backend (without restrictions, i.e. allowing all).
* The generated ports will be 5000 for http and 5001 for https, respectively.
* The connectionstring for the database is located in ```appsettings.json```
* Some log flags in ```appsettings.json``` are set to ```Warning```
* In ```launchsettings.json``` two settings are configured: one (the default) for the generated REST service, another one for swagger.

## Prerequesites
* .net6 SDK installed. Test with the following command (should show 6.0.100 or higher):
  ```
  dotnet --version
  ```
* tool dotnet-ef version 6 installed. Test with the following command (should show 6.0.0 or higher):
  ```
  dotnet-ef --version
  ```

## Usage

```
  createWebApiProject solutionName [pathToDbFile] [tableName] [targetFolder]
```
**Parameters**
|parameter   |optional|description|default |
|:----------:|:------:|:----------|:-------|
|solutionName|	      |Name of the solution and name of folder|
|pathToDbFile|    *   |If set, an Entity data model will be generated. Database has to reside in local folder or absolute path given.|
|tableName	 |    *   |For this table a Get-WebService is generated.|Categories|
|targetFolder|    *   |Folder where to generate the project to.|Subfolder with solution name in current folder|

### Examples
1. Create backend named MyBackend into a local subfolder without using a database
   ```
   createWebApiProject MyBackend
   ```
1. Create backend into a local subfolder using Northwind SQLServer database - generated REST service will use table Categories
   ```
   createWebApiProject NorthwindManager Northwnd.mdf
   ```
1. Create backend into a local subfolder using Northwind Sqlite database - generated REST service will use table Products
   ```
   createWebApiProject NorthwindManager Northwnd.sqlite Products
   ```
1. Create backend to C:\Temp using Northwind SQLServer database located in D:\Temp - generated REST service will use table Products
   ```
   createWebApiProject NorthwindManager C:\Temp\Northwnd.mdf Products D:\Temp
   ```